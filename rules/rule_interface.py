import abc


class RuleInterface:
    ITEM_NAME: str

    @abc.abstractmethod
    def get_value(self, address_name):
        pass
