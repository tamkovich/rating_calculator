from rules.rule_interface import RuleInterface


class ShopRule(RuleInterface):
    ITEM_NAME = "магазин"

    def get_value(self, address_name: str) -> int:
        items = get_items_from_google_api(address_name)
        return 1 if any(items) else 0
