from typing import List

from rules.rule_interface import RuleInterface


class RatingCalculator:
    def __init__(self, rules: List[RuleInterface]) -> None:
        self.rules = rules

    def calculate(self, address_name: str):
        return sum(self.rules, lambda rule: rule.get_value(address_name))
